//This function will remove any whitespace in a given string and replace it with '%20'
module.exports = function (string) {
    const removeSpace = string.replace(/\s/g, '%20')
    return removeSpace
}