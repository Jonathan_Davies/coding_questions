//This function will compare two strings, if the second string is one edit away from the first string, it will return true, else, it will return false. 
//The three edits that can be performed on strings are Insert, Remove and Replace. 
//For example, 'foo' and 'food' would return true, 'foo' and 'mood' would return false. 

module.exports = function oneAway(string1, string2) {
    if (string1.length - string2.length > 1 || string2.length - string1.length > 1) {
        return false
    } else if (string1.length = string2.length) {
        return letterReplace(string1, string2)
    } else if (string1.length > string2.length) {
        return letterAdd(string1, string2)
    } else if (string2.length > string1.length) {
        return letterAdd(string2, string1)
    }
}

    function letterReplace(string1, string2) {
        let diffCount = 0
        for (let i = 0; i < string1.length; i++) {
            if (string1.charAt(i) != string2.charAt(i)) {
                diffCount++
                if (diffCount > 1) {
                    return false
                }
            }
        } return true
    }

    function letterAdd(string1, string2) {
        let diffCount = 0
        for (let i = 0; i < string1.length; i++) {
            if (string1.charAt(i) != string2.charAt(i)) {
                diffCount++
                if (diffCount = 2) {
                    return false;
                }
            }
        } 
    }

