//This function should take a string of letters and return a compressed string of the unique letters in the string and the number of their occurences. 
//For example 'aabbccc' should print 'a2b2c3' to the console

module.exports = function stringCompression(string) {
    var newString = ''
    let letterCount = 0
    for (let i = 0; i < string.length; i++) {
      letterCount++
      if(string.charAt(i) != string.charAt(i + 1)) {
        newString += string.charAt(i) + letterCount
        letterCount = 0
      }
    }
    return newString
  }
  
 