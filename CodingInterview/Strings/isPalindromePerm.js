//This function will compare two given strings, comparing if the second string is a permutation of the first.
//For example, 'tat' and 'att' would return true.
module.exports = function (word1, word2) {
    if (word1.length != word2.length) {
        return false
    } else {
    word1Sort = word1.split('').sort().join()
    word2Sort = word2.split('').sort().join()
    return word1Sort === word2Sort
}}