//This function will compare two given strings, if string two is a rotated form of string one, it will return true, else, it will return false.
module.exports = function stringRotation(string1, string2) {
    if(string1.length > string2.length) {
        return false
    }
    if(string1.length == 0 || string2.length == 0) {
        return false
    }
    sortStr1 = string1.split("").sort().join()
    sortStr2 = string2.split("").sort().join()
    return sortStr1 == sortStr2
}