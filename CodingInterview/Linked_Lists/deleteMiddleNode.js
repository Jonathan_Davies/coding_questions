//This function will delete the middle node of a linked list, given the node.

module.exports = function deleteMiddle(index, list) {
    list.removeAtIndex(index);
};