//This function will take two lists, add them together, and return the sum in a new linked list. 
//The numbers are stored in reverse order, solve and return a single linked list, also in reverse order (912 being stored as 2 -> 1 -> 9)
const linkedList = require ('./linkedListPractice')

module.exports = function sumList(List1, List2) {
    List1Arr = []
    List2Arr = []
    

    for(let i = 0; i < List1.length; i++) {
        List1Arr.push(List1.getByIndex(i).value)
    }
    for(let i = 0; i < List2.length; i++) {
        List2Arr.push(List2.getByIndex(i).value)
    }
    const String = List1Arr.reverse().join('')
    const String2 = List2Arr.reverse().join('')
    const sum = (+String + +String2).toString().split('')
    
    
    List3 = new linkedList 

    for(let i = 0; i < sum.length; i++) {
        List3.insertAtHead(sum[i])

    }
    List3.print()
    

}