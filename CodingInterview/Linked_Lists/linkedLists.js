class Node {
    constructor(data, next = null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }

    insertFirst(data) {
        this.head = new Node(data, this.head)
        this.size++;
    }

    insertLast(data) {
        let node = new Node(data);
        let current;

        if(!this.head) {
            this.head = node
        } else {
          current = this.head;
          while(current.next) {
              current = current.next;
          } 
          current.next = node; 
        }

        this.size++
    }

    insertAt(data, index) {
        if(index > 0 && index > this.size) {
            return;
        }

        if (index === 0) {
            this.head = new Node(data, this.head)
            return;
        }

        const node = new Node(data);
        let current, previous;

        current = this.head;
        let count = 0;

        while(count < index) {
            previous = current; 
            count++;
            current = current.next;
        }

        node.next = current;
        previous.next = node;

        this.size++;
    }

    getAt(index) {
        let current = this.head;
        let count = 0;

        while(current) {
            if(count == index) {
                console.log(current.data);
            }
            count++;
            current = current.next;
        }
        return null;
    }

    removeAt(index) {
        if(index > 0 && index > this.size) {
            return;
        }

        let current = this.head;
        let previous;
        let count = 0;

        if(index === 0) {
            this.head = current.next;
        } else {
            while(count < index) {
                count++;
                previous = current;
                current = current.next;
            }

            previous.next = current.next;
        }
        this.size--;
    }

    // removeDuplicates() {
    //     let buffer = [];
       
    //     // for(let i = 0; i < this.size; i++) {
    //         while(current){
    //         if(!buffer.includes(this.getAt(i).data)) {
    //             buffer.push(i).data;
    //         } else {
    //             this.removeAt(i);
    //             this.size--;
    //         }
    //     }
    //     console.log(buffer);
    // }

    printListData() {
        let current = this.head;

        while(current) {
            console.log(current.data);
            current = current.next;
        }
    }

    clearList() {
        this.head = null;
        this.size = 0;
    }
}

const list = LinkedList.fromValues = function(...values) {
    const ll = new LinkedList()
    for ( let i = values.length - 1; i >= 0; i--) {
        ll.insertFirst(values[i])
    }
    return ll
}

const ll = new LinkedList();

// const ll = new LinkedList(1, 2, 3, 4);

// ll.insertFirst(100);
// ll.insertFirst(200);
// ll.insertFirst(300);
// ll.insertFirst(300);
// ll.insertLast(400);
// ll.getAt(10);
// ll.clearList();
// ll.removeAt(33);
ll.removeDuplicates(1, 2, 3, 3, 3, 4, 5);
ll.printListData();