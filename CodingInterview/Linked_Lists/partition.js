// This function will partition a Linked List around a value X, nodes containing values less than X will be at the beginning of the list, 
// while nodes greater than and including X will be at the end of the list.
const linkedList = require ('./linkedListPractice')
module.exports = function partition(value, list) {
    const lowerNum = new linkedList()
    const higherNum = new linkedList()
    for (let i = 0; i < list.length; i++) {
        if (list.getByIndex(i).value < value) {
            lowerNum.insertAtHead(list.getByIndex(i).value)
        } else { 
            higherNum.insertAtHead(list.getByIndex(i).value)
        }
    }
    function mergeLists(L1, L2) {
        var sortedList = new linkedList()
        for( let j = 0; j < L2.length; j++) {
            sortedList.insertAtHead(L2.getByIndex(j).value)
        }
        for( let i = 0; i < L1.length; i++) {
            sortedList.insertAtHead(L1.getByIndex(i).value)
        }
        // for(let i = 0; i < sortedList.length; i++) {
        //     console.log(sortedList.getByIndex(i).value)
        // }
    }
    mergeLists(lowerNum, higherNum)
    
}
