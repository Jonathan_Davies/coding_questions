module.exports = function isPalindrome(list) {

    listArr = []
    reverseArr = []

    for(let i = 0; i < list.length; i++) {
        listArr.push(list.getByIndex(i).value)
    }
    reverseArr = listArr.reverse().join('')
    listArr = listArr.reverse().join('')

   return listArr === reverseArr

}