const expect = require( "chai" ).expect;
const numOrder = require( "../Practice_files/numOrder.js" );

describe( "numOrder.js", function () {

    it( "returns min and max elements from array", function() {
      expect( numOrder([1, 2, 3, 4, 5])).to.deep.equal([1, 5]);      
    });

    it( "equal 0 matchsticks", function() {
      expect( numOrder([1, 2, 3])).to.deep.equal([1, 3]);
    })
  
    it( "does not equal matchsticks", function() {
      expect(numOrder([5])).to.not.equal([51]);
    })
    
  });