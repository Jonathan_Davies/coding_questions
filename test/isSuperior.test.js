const expect = require( "chai" ).expect;

const isSuperior = require( "../Practice_files/isSuperior.js" );

describe( "isSuperior.js", function () {

    it( "The string is superior", function() {
  
      expect( isSuperior([1, 2, 3, 4], [1, 2, 3, 3])).to.be.true;
      expect( isSuperior(['a', 'b', 'c'], ['a', 'd', 'c'])).to.be.false;
      expect( isSuperior([true, 10, 'zebra'], [true, 10, 'zebra'])).to.be.false;
      expect( isSuperior([9.1, 8.1, 7.1, 6.1], [9.1, 8.1, 7.1, 5.1])).to.be.true;
  
    });
  
  });
