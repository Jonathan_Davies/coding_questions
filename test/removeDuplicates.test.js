const expect = require( "chai" ).expect;
const removeDuplicates = require( "../Practice_files/removeDuplicates" );

describe( "removeDuplicates.js", function () {

    it( "removes duplicated entries form a string", function() {
      expect(removeDuplicates("aaabb")).to.deep.equal([ 'a', 'b' ]);      
    });
    it( "removes duplicated entries form a string", function() {
        expect(removeDuplicates("acb")).to.deep.equal(['a', 'c', 'b']);      
      });
  });