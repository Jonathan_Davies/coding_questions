const expect = require( "chai" ).expect;

const isPalindrome = require( "../Practice_files/isPalindrome" );

describe( "isPalindrome.js", function () {

  it( "The string is a palindrome", function() {

    expect( isPalindrome( "tenet" ) ).to.be.true;
    expect( isPalindrome( "race the car" ) ).to.be.false;
    expect( isPalindrome( "radar" ) ).to.be.true;
    expect( isPalindrome( "willow" ) ).to.be.false;

  });

});