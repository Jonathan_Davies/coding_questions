const { expect } = require('chai')
const partition = require('../CodingInterview/Linked_Lists/partition')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')

describe('partition', () => {
    it('should partition a linked list around a given value', function () {
        const ll = linkedList.fromValues(4, 2, 2, 3, 3, 2, 5, 3, 1)
        partition(3, ll)
    })
})