const expect = require("chai").expect;

const stringRotation = require("../CodingInterview/Strings/stringRotation.js")

describe("stringRotation.js", function() {
    it("will compare two strings, returning 'true' if they are rotated forms of each other ", function () {
        expect(stringRotation("house", "useho")).to.be.true
        expect(stringRotation("computer", "putercom")).to.be.true
        expect(stringRotation("ballfoot", "football")).to.be.true

    })
    it("will compare two strings, returning 'false' if they are not rotated forms of each other ", function () {
        expect(stringRotation("cat", "dog")).to.be.false
        expect(stringRotation("houses", "house")).to.be.false
        expect(stringRotation("", "")).to.be.false
        expect(stringRotation(" ", "")).to.be.false
        expect(stringRotation("", " ")).to.be.false

    })
})