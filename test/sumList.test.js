const { expect } = require('chai')
const sumList = require('../CodingInterview/Linked_Lists/sumList')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')


describe('sumList.js', () => {
    it('should add together two linked lists', () => {
        const ll = linkedList.fromValues(7, 1, 6)
        const ll2 = linkedList.fromValues(5, 9, 2)
        sumList(ll, ll2)
    })
})