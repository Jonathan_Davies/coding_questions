const expect = require("chai").expect;
const stringCompression = require("../CodingInterview/Strings/stringCompressions.js");

describe("springCompressions.js", function () {
    it("Compress a string into single letters and the number of their occurances", function () {
        expect(stringCompression('aabbcc')).to.equal('a2b2c2');
        expect(stringCompression('abc')).to.equal('a1b1c1');
        expect(stringCompression('abbccc')).to.equal('a1b2c3');
        expect(stringCompression('abc')).to.not.equal('abc');
        expect(stringCompression('')).to.not.equal('abc')
    });
});