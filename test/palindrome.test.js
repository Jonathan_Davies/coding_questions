const isPalindrome = require('../CodingInterview/Linked_Lists/palindrome')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')
const { expect } = require('chai')


describe('palindrome.js', () => {
    it('should check to see if a linked list contains a palindrome', () => {
        const ll = linkedList.fromValues(1, 2, 1)
        const ll2 = linkedList.fromValues(1, 2, 3)
        const ll3 = linkedList.fromValues(1, 2, 2, 3)
        const ll4 = linkedList.fromValues(1, 2, 2, 1)

        expect(isPalindrome(ll)).to.be.true
        expect(isPalindrome(ll2)).to.be.false
        expect(isPalindrome(ll3)).to.be.false
        expect(isPalindrome(ll4)).to.be.true
    })
})