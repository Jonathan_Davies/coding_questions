const { expect } = require('chai')
const linkedList = require('../CodingInterview/Linked_Lists/linkedListPractice')

describe('insertAtHead', () => {
    it('adds the element to the beginning of the list', () => {
        const ll = new linkedList()
        ll.insertAtHead(10)
        const oldHead = ll.head
        ll.insertAtHead(20)

        expect(ll.head.value).to.equal(20)
        expect(ll.head.next).to.equal(oldHead)
        expect(ll.length).to.equal(2)
    })
})

describe('getByIndex', () => {
    describe('with index less than 0', () => {
        it('returns null', () => {
            const ll = linkedList.fromValues(10, 20)

            expect(ll.getByIndex(-1)).to.be.null
        })
    })

    describe('with index greater than list length', () => {
        it('returns null', () => {
            const ll = linkedList.fromValues(10, 20)

            expect(ll.getByIndex(5)).to.be.null
        })
    })

    describe('with index 0', () => {
        it('returns the head', () => {
            const ll = linkedList.fromValues(10, 20)
            
            expect(ll.getByIndex(0).value).to.equal(10)
        })
    })

    describe('with index in the middle', () => {
        it('returns the element at the index', () => {
            const ll = linkedList.fromValues(10, 20, 30, 40)
            
            expect(ll.getByIndex(2).value).to.equal(30)
        })
    })
})

describe('insertAtIndex', () => {
    describe('with an index less than 0', () => {
        it('does not insert anything', () => {
            const ll = linkedList.fromValues(10, 20)
            ll.insertAtIndex(-1, 30)
            
            expect(ll.length).to.equal(2)
        })
    })

    describe('with an index greater than list length', () => {
        it('does not insert anything', () => {

            const ll = linkedList.fromValues(10, 20)
            ll.insertAtIndex(5, 30)
            
            expect(ll.length).to.equal(2)
        })
    })

    describe('with an index of 0', () => {
        it('should insert at head', () => {
            const ll = linkedList.fromValues(10, 20)
            ll.insertAtIndex(0, 30)
            expect(ll.head.value).to.equal(30)
            expect(ll.head.next.value).to.equal(10)
            expect(ll.length).to.equal(3)
    })

    describe('with an index in the middle', () => {
        it('should insert at the given index', () => {
            const ll = linkedList.fromValues(10, 20, 30, 40)
            ll.insertAtIndex(2, 50)
            const node = ll.getByIndex(2)
            expect(node.value).to.equal(50)
            expect(node.next.value).to.equal(30)
            expect(ll.length).to.equal(5)
    })
        })
    })
})

describe('removeAtHead', () => {
    it('removes the head', () => {
        const ll = linkedList.fromValues(10, 20, 30)
        ll.removeHead()

        expect(ll.head.value).to.equal(20)
        expect(ll.length).to.equal(2)
    })

})

describe('removeAtIndex', () => {
    describe('with an index less than 0', () => {
        it('does not remove anything', () => {
            const ll = linkedList.fromValues(10, 20)
            ll.removeAtIndex(-1)
            
            expect(ll.length).to.equal(2)
        })
    })

    describe('with an index greater than list length', () => {
        it('does not remove anything', () => {

            const ll = linkedList.fromValues(10, 20)
            ll.removeAtIndex(-1)
            
            expect(ll.length).to.equal(2)
        })
    })

    describe('with an index of 0', () => {
        it('should remove the head', () => {
            const ll = linkedList.fromValues(10, 20, 30)
            ll.removeAtIndex(0)
            expect(ll.head.value).to.equal(20)
            expect(ll.head.next.value).to.equal(30)
            expect(ll.length).to.equal(2)
    })

    describe('with an index in the middle', () => {
        it('should remove at the given index', () => {
            const ll = linkedList.fromValues(10, 20, 30, 40)
            ll.removeAtIndex(2)
            const node = ll.getByIndex(1)
            expect(node.value).to.equal(20)
            expect(node.next.value).to.equal(40)
            expect(ll.length).to.equal(3)
    })
        })
    })
})
