const { expect } = require('chai')
const removeDuplicates = require('../CodingInterview/Linked_Lists/removeDuplicates')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')

describe('removeDuplicates', () => {
    it('should remove duplicate elements in unsorted linked list', function () {
        const ll = linkedList.fromValues(10, 20, 20, 30, 30, 20, 30, 30, 40)
        removeDuplicates(ll)
    })
})