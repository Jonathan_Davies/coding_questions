const expect = require ("chai").expect;

const isPalindromePerm = require ("../CodingInterview/Strings/isPalindromePerm.js")

describe ( "isPalindromePerm.js", function () {

    it("String is permutation of palindrome", function () {
        expect( isPalindromePerm ( 'nurses run', 'nusres nur')).to.be.true;
        expect( isPalindromePerm( 'taco cat', 'cato tac')).to.be.true;
        expect( isPalindromePerm( 'taco cat', 'cato eec')).to.be.false;
    } )
})