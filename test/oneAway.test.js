const expect = require("chai").expect;

const oneAway = require("../CodingInterview/Strings/oneAway.js")

describe("oneAway.js", function() {
    it("will return 'true' if the two strings are one edit apart", function () {
        expect(oneAway("house", "houses")).to.be.true
        expect(oneAway("cats", "cat")).to.be.true
    })
    it("will return 'false' if the strings are more than one edit apart", function() {
        expect(oneAway("cat", "dog")).to.be.false
        expect(oneAway("house", "dog")).to.be.false
        expect(oneAway("dog", "house")).to.be.false
        expect(oneAway(" ", "123")).to.be.false
    })
})