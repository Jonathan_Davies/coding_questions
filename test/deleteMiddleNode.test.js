const { expect } = require('chai')
const linkedList = require('../CodingInterview/Linked_Lists/linkedListPractice')
const deleteMiddle = require ('../CodingInterview/Linked_Lists/deleteMiddleNode')


describe('delete middle index', () => {
    it('should remove at the given index', () => {
        const ll = linkedList.fromValues(10, 20, 30, 40)
        deleteMiddle(1, ll)
        const node = ll.getByIndex(0)
        expect(node.value).to.equal(10)
        expect(node.next.value).to.equal(30)
        expect(ll.length).to.equal(3)
        console.log(ll)
    })
})