const intersect = require('../CodingInterview/Linked_Lists/intersect')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')

describe('intersect.js', () => {
    it('should determine if two lists intersect, and return the intersecting node if true', () => {
        const ll = linkedList.fromValues(1, 2, 1)
        const ll2 = linkedList.fromValues(1, 2, 1, 3, 4)
        
        intersect(ll, ll2)
    })
})