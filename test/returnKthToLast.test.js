const { expect } = require('chai')
const returnKth = require('../CodingInterview/Linked_Lists/returnKthToLast')
const linkedList = require ('../CodingInterview/Linked_Lists/linkedListPractice')

describe('return Kth to last', () => {
    it('should return the Kth to last value of a linked list', function () {
        const ll = linkedList.fromValues(4, 2, 2, 3, 3, 2, 5, 3, 1)
        expect(returnKth(5, ll)).to.equal(3)
    })
})