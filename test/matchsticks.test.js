const expect = require( "chai" ).expect;

const matchsticks = require( "../Practice_files/matchsticks.js" );

describe( "matchsticks.js", function () {

    it( "equal positive matchsticks", function() {
      expect( matchsticks(1)).to.equal(6);      
    });

    it( "equal 0 matchsticks", function() {
      expect( matchsticks(0)).to.equal(0);
    })
  
    it( "does not equal matchsticks", function() {
      expect(matchsticks(5)).to.not.equal(51);
    })
    
  });

