module.exports = function isPalindrome(word) {
    const forwardWord = word
    const reverseWord = forwardWord.split('').reverse().join('')
    return forwardWord === reverseWord
    
}

