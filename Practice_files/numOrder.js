module.exports = function (arr) {
    const min = Math.min(...arr)
    const max = Math.max(...arr)
    // console.log(min, max)
    return [min, max]
}