//This function will remove duplicate elements from an unsorted list.
module.exports = function removeDuplicates(string) {
    checkedElements = []
    splitString = string.split("")
    for(let i = 0; i < string.length; i++) {
        if (!checkedElements.includes(string[i])) {
            checkedElements.push(string[i])
        } else {
            i++
        }
    }
    return checkedElements
} 
